from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm

# Create your views here.


def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/list.html", context)


# for the list of items to be grab
def list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": todo,
    }
    return render(request, "todos/detail.html", context)


# how to create a list
def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            # don't forget to have form.save() to actually save the input
            form.save()
            return redirect("todo_list")

    else:
        form = TodoForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


# how to edit a list
def edit_list(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            list = form.save()
            return redirect("edit_list", id=list.id)
    else:
        form = TodoForm(instance=todo)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


# how to delete a list
def delete_list(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list")
    return render(request, "todos/delete.html")
