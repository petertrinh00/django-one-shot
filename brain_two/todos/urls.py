from django.urls import path
from todos.views import (
    todo_list,
    list_detail,
    create_list,
    edit_list,
    delete_list,
)

urlpatterns = [
    path("", todo_list, name="todo_list"),
    path("<int:id>/", list_detail, name="list_detail"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
]
